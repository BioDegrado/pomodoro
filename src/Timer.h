#ifndef TIMER_H
#define TIMER_H

#include <Arduino.h>
#include "Screen.h"
#include "Filler.h"

/**
 * @brief It handles and draws a timer onto the "Screen".
 *
 * The Timer class handles a timer as well as the graphical components of it.
 * It shows the time, plus "filler" rectangle that signals the percentage until the next tick.
 * So for hours it represents minutes, for minutes it represents seconds and so on.
 * */
class Timer {
	private:

		float time; // Time in seconds
		int start; // Starting time, in seconds
		int height; // Distance from top
		Screen * screen; // Screen where to draw
		Filler * filler; // A graphical filler for the timer, just to make it prettier

	public:
		Timer(int time, int height, Screen * screen);

		/**
		 * Checks if the timer has been completed
		 *
		 * @return if the timer has reached 0 
		 */
		bool isDone();

		/**
		 * Updates the time with the elapsed time.
		 */
		void update(float delta); // delta time in millis

		/**
		 * Resets the timer
		 */
		void reset();

		/**
		 * It returns an interval [0..1] of the completition percentage of the current tick.
		 * If there are still hours left, it counts the minutes passed within it.
		 * If there are still minutes left, then it counts the seconds, and so on.
		 *
		 * @return the completition percentage of the bigger tick "hours->mins, mins->secs, secs->millis"
		 */
		const float tickProgress();

		/**
		 * Draw everything on screen.
		 * */
		void draw();
};

#endif

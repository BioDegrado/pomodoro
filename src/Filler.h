#ifndef FILLER_H
#define FILLER_H

#include <Arduino.h>

#include "Screen.h"

/**
 * @brief It's the class handles Screen filling (rectangle) with either "on" leds, or "off" leds. 
 * */
class Filler {
	private:
		Screen * screen;
		int top; // Top part (remember the y axis is inversed).
		int bot;
		bool on;
		float perc;
		byte fill[3] = {0x00, 0x00, 0x00};
	public:
		Filler(int top, int bot, bool on, Screen * screen);

		/**
		 * @brief updates the filler with the percentage that fill is covering.
		 * @param perc is the percentage [0..1 interval] of coverage of the filler.
		 * */
		void update(float perc);

		void draw();
};

#endif

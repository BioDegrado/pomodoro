#include "Filler.h"
#include <Arduino.h>

Filler::Filler(int top, int bot, bool on, Screen * screen) {
	this->top = top;
	this->bot = bot;
	this->on = on;
	this->screen = screen;
	this->perc = 100;
	if (on) for (int i = 0; i < 3; i++) fill[i] = 0xFF; // if the bit is "1" then the filler is all 1s
}

void Filler::update(float perc) { this->perc = perc; }

void Filler::draw() {
	int cap = (bot - top) * perc;
	for (int row = bot; row > cap; row--)
		screen->flipmap(nullptr, fill, 1, 3, 0, row);
}

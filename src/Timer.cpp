#include "Timer.h"

#include <Arduino.h>

#include "Screen.h"
#include "Images/numbers.h"
#include "Images/letters.h"
#include "Filler.h"

Timer::Timer(int time, int height, Screen * screen) {
	this->time = time;
	this->height = height;
	this->screen = screen;
	start = time;
	filler = new Filler(0, 32, true, screen);
}

void Timer::update(float delta) {
	if (time >= 0)
		time -= delta / 1000;
}

bool Timer::isDone() { return time <= 0; }

const float Timer::tickProgress() {
	int seconds = time;
	int minutes = time / 60;
	int hours = time / (60 * 60);
	if (hours)
		return (float)(minutes % 60)/ 60.f;
	else if (minutes)
		return (float)(seconds % 60) / 60.f;
	else 
		return time - seconds;

}

void Timer::reset() {
	time = start;
}


void Timer::draw() {
	int seconds = time;
	int minutes = time / 60;
	int hours = time / (60 * 60);
	if (hours) {
		int d1 = hours / 10;
		int d2 = hours % 10;
		screen->flipmap(nullptr, &Image::NUMBERS[d1][0][0], 12, 1, 0, height);
		screen->flipmap(nullptr, &Image::NUMBERS[d2][0][0], 12, 1, 8, height);
		screen->flipmap(nullptr, &Image::H[0][0], 4, 1, 16,8 + height);
	} else if (minutes){
		int d1 = minutes / 10;
		int d2 = minutes % 10;
		screen->flipmap(nullptr, &Image::NUMBERS[d1][0][0], 12, 1, 0, height);
		screen->flipmap(nullptr, &Image::NUMBERS[d2][0][0], 12, 1, 8, height);
		screen->flipmap(nullptr, &Image::M[0][0], 4, 1, 16, 8 + height);
	} else {
		int d1 = seconds / 10;
		int d2 = seconds % 10;
		screen->flipmap(nullptr, &Image::NUMBERS[d1][0][0], 12, 1, 0, height);
		screen->flipmap(nullptr, &Image::NUMBERS[d2][0][0], 12, 1, 8, height);
		screen->flipmap(nullptr, &Image::S[0][0], 4, 1, 16, 8 + height);
	}
	filler->update(tickProgress());
	filler->draw();
}

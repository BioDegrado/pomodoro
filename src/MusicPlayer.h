#ifndef MUSIC_PLAYER_H
#define MUSIC_PLAYER_H

/**
 * @brief the class behaves as a music player.
 *
 * This is a very primitive "music player" produces music by playing only one melody at only one volume.
 * Unfortunately for slow devices it performes worse, since it will skips notes (like mine device!)
 * */
class MusicPlayer {

	private:
		int * notes; // List of notes
		int * durations; // List of "durations" (music conventions).
		int n;
		int pin;
		float idx = 0; // The note to play
		float waitTime = 0; // Waiting time between notes.

	public:
		/**
		 * @param notes list of notes (frequencies)
		 * @param durations list of durations (same length as "notes")
		 * @param n number of notes
		 * @param pin buzzer's pin
		 */
		MusicPlayer(int * notes, int * durations, int n, int pin);

		/**
		 * sets the device ready for playing.
		 */
		void play();

		/**
		 * plays the device, must be called every cycle, otherwise it will keep sounding the
		 * same note!
		 */
		void update(float delta);

		void stop();
};

#endif

#ifndef SCREEN_H
#define SCREEN_H

#include "LedControl/LedControl.h"

/**
 * It's the class that handles a series of 3 DOT MATRICES as a single screen.
 * It takes as input a continuous array of memory as a binary image. And it appends
 * to the "buffer". When the final image is complete then the "draw()" function can 
 * be called to output the final image to the screens.
 *
 * To avoid any posssible artifacts the function "clear_buffer()" must be called before
 * composing a new image, otherwise it will work on top of the old one.
 *
 * */
class Screen {
	private:
		LedControl ledControls[3]; // The handlers of the 3 strips of dot matrices.
		byte buffer[12][8]; // The buffer, or "Canvas".

		/**
		 * @return the address of the dot matrix that contains the point.
		 */
		int getAddr(int x, int y);

		/**
		 * @return the dot matrix index that contains the point.
		 */
		int getStrip(int x, int y);

		/**
		 * @return the row of the dot matrix that contains the point.
		 */
		int getRow(int x, int y);

		/**
		 * @return given the byte int the buffer that contains the point, it returns its position in the byte.
		 */
		int getPos(int x, int y);

		/**
		 * @return the address of the byte that contains the point. 
		 */
		byte * getBufferCell(int x, int y);

		/**
		 * @return the bit of the buffer in that position.
		 */
		bool getBufferBit(int x, int y);

		/**
		 * assign a bit to the buffer to the given position.
		 */
		void assignBuffer(bool bit, int x, int y);

		/**
		 * performs a xor operation on the buffer using the given position.
		 */
		void xorBuffer(bool bit, int x, int y);
	public:

		/**
		 * @brief it takes the 3 pins of the screens in the following order {left_pin, mid_pin, right_pin}, and then the intensity.
		 * @param dataPin the 3 data pins.
		 * @param clkPin the 3 clock pins.
		 * @param csPin the 3 chip select pins.
		 * @param intensity the light intensity of the screen (currently not working).
		 */
		Screen(int dataPin[3], int clkPin[3], int csPin[3], int intensity = 8);

		void setIntensity(int intensity);

		/**
		 * @brief It writes the given image to the buffer.
		 * @param mask It is the mask (has to be the same size of "img") that decides which
		 * part of the image must be used to overlap. If it is "nullptr", then it is set as mask = img.
		 * @param img the image to write to the buffer.
		 * @param r rows of the image.
		 * @param c columns of the image.
		 * @param offx the "x" coordinate on where to write the image (it points to the top left corner).
		 * @param offy the "y" coordinate on where to write the image (it points to the top left corner).
		 */
		void overlap(byte * mask, byte * img, int r, int c, int offx, int offy);

		/**
		 * @brief It applies the given image to the buffer as a XOR operation.
		 * @param mask It is the mask (has to be the same size of "img") that decides which
		 * part of the image must be used. If it is "nullptr", then it is set as mask = img.
		 * @param img the image to use.
		 * @param r rows of the image.
		 * @param c columns of the image.
		 * @param offx the "x" coordinate on where to write the image (it points to the top left corner).
		 * @param offy the "y" coordinate on where to write the image (it points to the top left corner).
		 */
		void flipmap(byte * mask, byte * img, int r, int c, int offx, int offy);

		/**
		 * @brief draws the buffer to the screen.
		 */
		void draw();

		/**
		 * @brief sends a black image to the screen.
		 */
		void clear_display();

		void clear_buffer();
};

#endif

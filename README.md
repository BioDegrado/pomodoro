# Yet Another Timer (YAT)
The project is a "pomodoro timer", that is a timer to help students in setting specific time intervals in which they can study, and take breaks.
This is version uses a rudimental display to show the time with additional graphics.
To interface with the device, 2 buttons are present: one pauses the device, the other resets the timer.

Although the project is simple, while lacking some functionalities (like setting a desired time), it is still a good project to try to learn some concepts:
* Simple animation handling.
* Delta time: for processing in real time regardless of clock speed.
* Basic applications of bit operations.
* Simple automatas.

## Hardware
YAT implements this timer on the ARDUINO R3 board, using the following peripherals:
* 3 [MAX7219 Dot matrices](https://www.amazon.it/AZDelivery-%E2%AD%90%E2%AD%90%E2%AD%90%E2%AD%90%E2%AD%90-MAX7219-Modulo-Arduino/dp/B07HJDV3HN/ref=sr_1_5?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&sr=8-5&th=1).
* A passive buzzer.
* 2 switches.
* A bunch of cables, and maybe a breadboard.

### Circuit
The circuit of the project is simple to setup:

\image html img/circuit.png

### Configuration
Although the behavior of the program is almost immutable, it can still be configured on compilation, thanks to the the configuration file "config.h", for example the pins can be rearrenged from the file.

## Images
The images used by YAT are hand made, and for space usage concerns, are arranged in a continuous array of bytes.
 The zeroes meaning that the led is off in that position, while the 1 is on.

```c++
		{
			{B00111100},
			{B01111110},
			{B11100111},
			{B11000111},
			{B11001011},
			{B11001011},
			{B11010011},
			{B11010011},
			{B11100011},
			{B11100111},
			{B01111110},
			{B00111100}
		},
		// This represents the number "0".
```

> Using booleans is not adviced, since each "bool" takes at least 1 byte of space, even if it can assume only 2 values.

So in mine implementation each byte can contain 8 pixels.
 The way it is handled is determined by the Screen class.

## The main classes
The basic code is mostly self-explanatory, but for a better understanding is better to look at the following classes first:

#### The Screen
The "LedController" class is from a library borrowed [here](https://github.com/wayoda/LedControl).
Which it deals with only one led matrix, and its way of handling the coordinate system is only adequate if the matrix is positioned horrizontally... which is not true in this case.
So the Screen class has to handle a transposed coordinate system, plus other problems relative to handling multiple screens as one.

#### The Timer
The Timer is the main focus of the project. It handles multiple tasks, besides counting the time. It has to draw on screen the numbers and the Filler, while also signaling when it has reached 0.

#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "Screen.h"
#include <Arduino.h>

/**
 * @brief It animates images using the "Screen" and a continuos segment of images.
 *
 * this class handles animations, 
 * with a continuous segment of memory, 
 * where it is divided into frames->cols->rows.
 * */
class Animator {
	private:
		byte * sprites;
		byte * masks;
		float idx;
		float speed;
		int rows;
		int cols;
		int n;
		int x;
		int y;
		Screen * screen;
	public:

		/**
		 * @param sprites the images to animate.
		 * @param rows rows of each image.
		 * @param cols columns of each image.
		 * @param n number of frames.
		 * @param x "x" coordinate of the position of the sprite on screen.
		 * @param y "y" coordinate of the position of the sprite on screen.
		 * @param speed speed of the animation.
		 * @param mask the bits of the image to render.
		 * */
		Animator(
			byte * sprites,
			int rows,
			int cols,
			int n,
			int x,
			int y,
			float speed,
			Screen * screen,
			byte * masks = nullptr
		);

		/**
		 * Chooses the next frame of animation given the elapsed time.
		 * @param delta variation of time since the last call.
		 * */
		void update(float delta);

		/**
		 * Draws the right frame processed in the previous call of update.
		 */
		void draw();
};

#endif

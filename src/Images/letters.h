#include <Arduino.h>

namespace Image {
	const byte H[4][1] = {
		{B01001000},
		{B01111000},
		{B01001000},
		{B01001000}
	};

	const byte M[4][1] = {
		{B00000000},
		{B00101000},
		{B01111100},
		{B01010100}
	};

	const byte S[4][1] = {
		{B00010000},
		{B00011000},
		{B00100100},
		{B01001000}
	};
};

/*
  Name: Yet Another Timer (YAT)
  Author: Xu Ruixuan (1909991)
  Date: 30/11/2021
  Desc: The project is a "pomodoro timer", that is a timer to help students in setting specific time intervals in which they can study, and take breaks.
  This is version uses a rudimental display to show the time with additional graphics.
  To interface with the device, 2 buttons are present: one pauses the device, the other resets the timer.
  Although the project is simple, while lacking some functionalities (like setting a desired time), it is still a good project to try to learn some concepts:
  Simple animation handling.
  Delta time: for processing in real time regardless of clock speed.
  Basic applications of bit operations.
  Simple automatas.

  PS:
  1. The project is hosted on GitLab: "https://gitlab.com/BioDegrado/pomodoro".
  2. For quick configurations, one has to modify the "config.h" file.
  3. More infos are in the DOC file "doc/html/index.html".
*/

#include "src/Screen.h"
#include "src/Timer.h"
#include "src/Filler.h"
#include "src/Animator.h"
#include "src/Button.h"
#include "src/MusicPlayer.h"

// Assets
#include "src/Images/book.h"
#include "src/Images/break.h"
#include "src/Images/pause.h"
#include "src/Audio/Pirates.h"

#include "config.h"

// Global states
typedef enum {
  RUNNING, PAUSE, RINGING, DONE, TEST
} State;

// Initializing objects
// Time
float now = millis();
float prev = now;
float delta = now - prev;

Screen screen = Screen(SCR_DIN, SCR_CLK, SCR_CS, INTENSITY);

Timer timer = Timer(TIME, 8, &screen);
Animator book = Animator(&Sprite::BOOK[0][0][0], 8, 2, 5, 8, 24, 0.003, &screen, &Sprite::B_MASK[0][0][0]);
Animator smoke = Animator(&Sprite::SMOKE[0][0][0], 3, 2, 4, 3, 2, 0.002, &screen);

MusicPlayer ringtone = MusicPlayer(Pirates::notes, Pirates::durations, 90, BUZZER_PIN);

Button resetButton = Button(RESET_PIN);
Button pauseButton = Button(PAUSE_PIN);

State state = RUNNING; // Starting state

void setup() {
  Serial.begin(9600); // In case for debugging
}

// It processes the delta time, so that each component can have infos about
// how much time has passed, therefore no component has a dependency to the clock speed.
void proc_time() {
  prev = now;
  now = millis();
  delta = TIME_SPEED * (now - prev);
}

void loop() {
  proc_time();
  // The screen has a buffer, where the final image is stored. Every loop we have to clean and then redraw the frame.
  screen.clear_buffer();

  // Resetting is an event indipendent of states, so that we can reset any time.
  if (resetButton.isPressed()) {
    timer.reset();
    ringtone.stop();
    state = RUNNING;
  }

  switch (state) {
    case RUNNING:
      timer.update(delta);
      book.update(delta);
      timer.draw();
      book.draw();

      if (timer.isDone()) {
        ringtone.play();
        state = RINGING;
      }
      if (pauseButton.isPressed())
        state = PAUSE;
      break;

    case PAUSE:
      timer.draw();
      book.draw();
      screen.overlap(&Image::P_MASK[0][0], &Image::PAUSE[0][0], 7, 3, 0, 0);
      if (pauseButton.isPressed()) state = RUNNING;
      break;

    case RINGING:
      ringtone.update(delta);
      smoke.update(delta);
      screen.overlap(nullptr, &Image::COFFE[0][0], 11, 2, 4, 6);
      screen.overlap(nullptr, &Image::BREAK[0][0], 11, 3, 2, 19);
      smoke.draw();
      if (pauseButton.isPressed()) {
        ringtone.stop();
        state = DONE;
      }
      break;

    case DONE:
      smoke.update(delta);
      ringtone.stop();
      screen.overlap(nullptr, &Image::COFFE[0][0], 11, 2, 4, 6);
      screen.overlap(nullptr, &Image::BREAK[0][0], 11, 3, 2, 19);
      smoke.draw();
      break;
    default:
      break;
  }

  screen.draw(); // After the buffer is ready we can draw.
}

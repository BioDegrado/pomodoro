#include "Animator.h"
#include "Screen.h"
#include <Arduino.h>
#include <math.h>

Animator::Animator(
	byte * sprites,
	int rows,
	int cols,
	int n,
	int x,
	int y,
	float speed,
	Screen * screen,
	byte * masks
) {
	this->sprites = sprites;
	this->masks = masks;
	this->rows = rows;
	this->cols = cols;
	this->n = n;
	this->x = x;
	this->y = y;
	this->screen = screen;
	this->idx = 0;
	this->speed = speed;
}

void Animator::update(float delta) { idx = fmod(idx + delta * speed, n); } // Move the idx forwards
void Animator::draw() {
	int i = ((int)idx) * rows * cols; // The next frame is (rows * cols)(bytes) away from the curr. frame
	byte * frame = &sprites[i];
	if (masks != nullptr) // Without a mask array we can't dereference!
		screen->overlap(&masks[i], frame, rows, cols, x, y);
	else
		screen->overlap(nullptr, frame, rows, cols, x, y);
}

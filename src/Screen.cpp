#include "Screen.h"

#include <inttypes.h>
#include <Arduino.h>

#include "LedControl/LedControl.h"

Screen::Screen(int dataPin[3], int clkPin[3], int csPin[3], int intensity) {
	for (int i = 0; i < 3; i ++) {
		ledControls[i] = LedControl(dataPin[i], clkPin[i], csPin[i], 4);
		for (int j = 0; j < 4; j++)
			ledControls[i].shutdown(j, false); // Activates every strip, otherwise they are unresponsive.
	}
	setIntensity(intensity);
	clear_buffer();
	clear_display();
}

/*
 * These number are strictly related to the way the buffer is organized.
 * The coordinate systems of the strips are transposed, so the buffer has to
 * be mapped with the right points in order to work.
 *
 * Since the buffer is a continuous segment of memory that goes downwards, and then
 * left; then the resulting mapping is as follows.
 */
int Screen::getAddr(int x, int y) { return y / 8; }
int Screen::getStrip(int x, int y) { return x / 8; }
int Screen::getRow(int x, int y) { return 7 - x % 8; }
int Screen::getPos(int x, int y) { return 7 - y % 8; }

byte * Screen::getBufferCell(int x, int y) {
	int addr = getAddr(x, y);
	int strip = getStrip(x, y);
	int row = getRow(x, y);
	return &buffer[addr + (strip * 4)][row];
}

bool Screen::getBufferBit(int x, int y) {
	return bitRead(*getBufferCell(x, y), getPos(x, y));
}

void Screen::assignBuffer(bool bit, int x, int y) {
	byte * cell = getBufferCell(x, y);
	int pos = getPos(x, y);
	bitWrite(*cell, pos, bit);
}

void Screen::xorBuffer(bool bit, int x, int y) {
	byte * cell = getBufferCell(x, y);
	int pos = getPos(x, y);
	bitWrite(*cell, pos, bit ^ bitRead(*cell, pos));
	
}

void Screen::overlap(byte * mask, byte * img, int r, int c, int offx, int offy) {
	mask = mask == nullptr ? img : mask;
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			int cell = i*c+j;
			for (int p = 0; p < 8; p++) {
				int x = offx + p + (j * 8);
				int y = offy + i;
				if (x > 8*3 -1 || x < 0 || y > 8*4 -1 || y < 0)
					continue;
				if (bitRead(mask[cell], 7-p)) {
					bool bit = bitRead(img[cell], 7-p);
					assignBuffer(bit, x, y);
				}
			}
		}
	}
}

void Screen::flipmap(byte * mask, byte * img, int r, int c, int offx, int offy) {
	mask = mask == nullptr ? img : mask; // If the mask is nullptr, then the 1 bits are used.
	for (int i = 0; i < r; i++) {
		for (int j = 0; j < c; j++) {
			int cell = i*c+j;
			for (int p = 0; p < 8; p++) {
				int x = offx + p + (j * 8);
				int y = offy + i;
				if (x > 8*3 -1 || x < 0 || y > 8*4 -1 || y < 0)
					continue;
				if (bitRead(mask[cell], 7-p)) { // [7 - p] the bits are read (RMB to LMB), while the inverted is needed.
					bool bit = bitRead(img[cell], 7-p); // [7 - p] the bits are read (RMB to LMB), while the inverted is needed.
					xorBuffer(bit, x, y);
				}
			}
		}
	}

}

void Screen::draw() {
	for (int i = 0; i < 12; i++) {
		int strip =  i / 4;
		int addr = 3 - (i % 4);
		for (int row = 0; row < 8; row++) {
			ledControls[strip].setRow(addr, row, buffer[i][row]);
		}
	}
	setIntensity(8);
}

void Screen::clear_display() {
	for (int i = 0; i < 32; i++)
		for (int j = 0; j < 3; j++) {
			int addr = 3 - (i / 8);
			int row = i % 8;
			ledControls[j].setRow(addr, row, 0x00);
		}
}

void Screen::clear_buffer() {
	for (int i = 0; i < 12; i++)
		for (int row = 0; row < 8; row++)
			buffer[i][row] = 0x00;
}

void Screen::setIntensity(int intensity) {
	for (auto lc : ledControls)
		for (int i = 0; i < 4; i++)
			lc.setIntensity(i, intensity);
}

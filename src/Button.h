#ifndef BUTTON_H
#define BUTTON_H

#include <Arduino.h>

/**
 * @brief It's the class that handles buttons, 
 * It implements 3 main features:
 *  1. Check button presses.
 *  2. Check button holding.
 *  3. Check button releases.
 * */
class Button {
	private:
		int prev_state;
		int pin;
		void registerKey(int state) { prev_state = state; }
	public:
		Button(int pin) {
			this->pin = pin;
			prev_state = HIGH;
			pinMode(pin, INPUT_PULLUP);
		}

		bool isPressed() {
			int state = digitalRead(pin);
			if (prev_state == HIGH && state == LOW) {
				registerKey(state);
				return true;
			}
			registerKey(state);
			return false;
		}

		bool isReleased() {
			int state = digitalRead(pin);
			if (prev_state == LOW && state == HIGH) {
				registerKey(state);
				return true;
			}
			return false;
		}

		bool isHolding() {
			int state = digitalRead(pin);
			if (prev_state == LOW && state == LOW) {
				registerKey(state);
				return true;
			}
			return false;
		}
};

#endif

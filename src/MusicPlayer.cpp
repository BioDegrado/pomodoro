#include "MusicPlayer.h"
#include "../config.h"

#include <math.h>
#include <Arduino.h>

MusicPlayer::MusicPlayer(int * notes, int * durations, int n, int pin) {
	this->notes = notes;
	this->durations = durations;
	this->n = n;
	this->pin = pin;
}

void MusicPlayer::play() {
	idx = 0;
	waitTime = 0;
}

void MusicPlayer::update(float delta) {
	waitTime -= delta / 1000; // updates the wait time
	float duration = MUSIC_DUR / (float)durations[(int)idx]; // Music duration conversion in millis 
	float speed = 1 / (duration + duration * MUSIC_TEMPO); // Adds some silent interval, for decreasing the tempo.
	idx = fmod(idx + (delta / 1000) * speed, n); // Moves "idx" forward

	if (waitTime > 0) return; // Exits if I still need to wait
		
	duration = MUSIC_DUR / (float)durations[(int)idx]; // Music duration conversion in millis 
	speed = 1 / (duration + duration * MUSIC_TEMPO); // Adds some silent interval, for decreasing the tempo.

	waitTime = duration + duration * 0.8; // Update the waitTime
	tone(pin, notes[(int)idx], duration * 1000); // Play the note!
}

void MusicPlayer::stop() {
	waitTime = 0;
	noTone(pin);
}

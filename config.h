#ifndef CONFIG_H
#define CONFIG_H

/*
 * Config file. Modify this file for setting all the configuration parameters.
*/

// The right screen pins.
#define R_DIN 11
#define R_CS 12
#define R_CLK 13

// The middle screen pins.
#define M_DIN 8
#define M_CS 9
#define M_CLK 10

// The left screen pins.
#define L_DIN 5
#define L_CS 6
#define L_CLK 7

#define RESET_PIN 4 // Reset button
#define PAUSE_PIN 3 // Pause button
#define BUZZER_PIN 2


#define TIME_SPEED 1
#define TIME 25* 60 // Time of the timer in seconds
#define INTENSITY 1 // Light intensity

#define MUSIC_DUR 0.5 // Music note duration
#define MUSIC_TEMPO 1.05

const int SCR_DIN[3] = {L_DIN, M_DIN, R_DIN};
const int SCR_CLK[3] = {L_CLK, M_CLK, R_CLK};
const int SCR_CS[3] = {L_CS, M_CS, R_CS};

#endif
